//
//  Copyright © 2018 Itty Bitty Apps Pty Ltd. All rights reserved.
//

import Core
import Foundation

struct AccumulatedPointCloud {

    var count: Int {
        return self.points.count
    }

    init() {
        let baseCapacity = 10000
        self.points.reserveCapacity(baseCapacity)
        self.colors.reserveCapacity(baseCapacity)
        self.identifiedIndices.reserveCapacity(baseCapacity)
    }
    
   func write(url: URL, text: String) -> Bool {
       guard let stream = OutputStream(url: url, append: true) else {
           return false
       }
       stream.open()
       
       defer {
           stream.close()
       }
       
       guard let data = text.data(using: .utf8) else { return false }
       
       let result = data.withUnsafeBytes {
           stream.write($0, maxLength: data.count)
       }
       return (result > 0)
   }

    mutating func appendPointCloud(_ pointCloud: PointCloud) {
        let identifiers = pointCloud.identifiers
        let points = pointCloud.points
        let colors = pointCloud.colors
//        let file = "tmp.txt"
        //    let file = FileHandle(forWritingAtPath: fileName)!

        for index in (0 ..< Int(pointCloud.count)) {
            let identifier = identifiers[index]
            let point = points[index]
//            let pointdata = "point"+" "+point.description
            print("point", point)
//            let data = pointdata.data(using: .utf8, allowLossyConversion: false)!
//           if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first{
//                let fileURL = dir.appendingPathComponent(file)
//                print("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh", fileURL.description)
//                if let fileHandle = try? FileHandle(forUpdating: fileURL){
//                    fileHandle.seekToEndOfFile()
//                    fileHandle.write(data)
//                    fileHandle.closeFile()
//                }
//            }
     

            let color = colors[index]

            if let existingIndex = self.identifiedIndices[identifier] {
                self.points[existingIndex] = point
                self.colors[existingIndex] = color
            } else {
                self.identifiedIndices[identifier] = self.points.endIndex
                self.points.append(point)
                self.colors.append(color)
            }
        }
    }

    func withPointAndColorBuffers<R>(_ body: (UnsafeBufferPointer<simd_float3>, UnsafeBufferPointer<simd_float3>) throws -> R) rethrows -> R {
        return try self.points.withUnsafeBufferPointer { points in
            return try self.colors.withUnsafeBufferPointer { colors in
                return try body(points, colors)
            }
        }
    }

    private var points = ContiguousArray<simd_float3>()
    private var colors = ContiguousArray<simd_float3>()
    private var identifiedIndices = [UInt64: Int]()

}






