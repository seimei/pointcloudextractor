
from alfred.vis.pointcloud.pointcloud_vis import draw_pcs_open3d
from alfred.fusion.common import draw_3d_box, compute_3d_box_lidar_coords
from alfred.fusion.kitti_fusion import load_pc_from_file

import open3d as o3d
import numpy as np
from extractPointCloud import  Extractor
import time

from GaussianMixtureModel import CenterPointDetector


data, data_cam,data_w, center, new_list, new_list_camera, final_frustum, camera_angle, world_position = Extractor('generator').extractPoints()

print(len(data),len(data_cam), len(data_w),len(center), len(final_frustum), len(world_position))



#geo=[]
#for i in range(len(new_list)):
#    pointcloud = o3d.PointCloud()
#    pointcloud.points = o3d.Vector3dVector(new_list[i])
#    pointcloud.paint_uniform_color([0, 0.651, 0.929])
#    geo.append(pointcloud)


geometry = o3d.geometry.PointCloud()
#geometry.points = o3d.utility.Vector3dVector(new_list[0])
geometry_camera = o3d.geometry.PointCloud()
geometry_center = o3d.geometry.PointCloud()
#geometry_line = o3d.geometry.PointCloud()
geometry_camera_angle = o3d.geometry.PointCloud() # camera_angle
geometry_world_position = o3d.geometry.PointCloud() # world position
geometry_object_center = o3d.geometry.PointCloud() # object center

# Animation
vis = o3d.visualization.Visualizer()
vis.create_window()
opt = vis.get_render_option()
opt.point_size = 6.0

line_set_ = o3d.geometry.LineSet() # for drawing object center
line_set = o3d.geometry.LineSet() # for drawing camera angle
line_set_far = o3d.geometry.LineSet() # for drawing camera angle zFar

while True:
    for i in range(len(new_list)):

        # point cloud 
        if i == 0:
            new_ = np.array(new_list[i], dtype=np.float64) # point cloud 
            new_l = np.array(new_list_camera, dtype=np.float64)[i] # camera position
            cameraAngle = np.array(camera_angle, dtype=np.float64)[i] # camera angle
            world = np.array(world_position, dtype=np.float64)[i] # world position
        else:
            new_ = np.concatenate([new_,np.array(new_list[i], dtype=np.float64)],axis=0) # point cloud 
            new_l = np.concatenate([new_l, np.array(new_list_camera, dtype=np.float64)[i]],axis=0) # camera position
            cameraAngle = np.concatenate([cameraAngle, np.array(camera_angle, dtype=np.float64)[i]], axis=0) # camera angle
            world = np.concatenate([world, np.array(world_position, dtype=np.float64)[i]], axis=0) # world position
        
#        new_l = np.array(new_list_camera, dtype=np.float64)[i]

        # get center of target object point clooud and camera position 
        #camera_position = np.array([float(np.mean(new_l[:,0])),float(np.mean(new_l[:,1])),float(np.mean(new_l[:,2]))], dtype=np.float64)
        camera_position = np.array([new_l[-1,0], new_l[-1,1], new_l[-1,2]], dtype=np.float64)
        #center
        cameraAngle_= np.array([float(np.mean(new_[:,0])), float(np.mean(new_[:,1])), float(np.mean(new_[:,2]))], dtype=np.float64) # object center
        # 5つある特徴点の中央値
        #center= np.array([float(np.mean(cameraAngle[:,0])), float(np.mean(cameraAngle[:,1])), float(np.mean(cameraAngle[:,2]))],dtype=np.float64)
        # 5つある末尾の特徴点
        center = np.array([cameraAngle[-1,0], cameraAngle[-1,1], cameraAngle[-1, 2]],dtype=np.float64)


        xFov = final_frustum[i,0,0]
        yFov = final_frustum[i,0,1]
        zNear = final_frustum[i,0,2]
        zFar = final_frustum[i, 0, 3]
        print("zNear", xFov,yFov,zNear, zFar)
        print("center", center)



        distance = np.linalg.norm(camera_position-center)
        print("distance", distance)
        ratio = -1*zNear/distance
        print("ratio", ratio)
        

        # 点群データの中心座標をとる
        Detector=CenterPointDetector(data=new_, zNear=zNear)
        result=Detector.KMeans()
        print("centerrrrrrrrrrrrrrrrrr",result)


        # create geometries
        geometry.points = o3d.utility.Vector3dVector(new_)
        geometry_camera.points = o3d.utility.Vector3dVector(new_l)
        geometry_center.points = o3d.utility.Vector3dVector([center])
        geometry_camera_angle.points = o3d.utility.Vector3dVector(cameraAngle)
        geometry_world_position.points = o3d.utility.Vector3dVector(world)
        geometry_object_center.points = o3d.utility.Vector3dVector(result)


        # paint color
        geometry.paint_uniform_color([0, 0.651, 0.929])
        geometry_camera.paint_uniform_color([1,0,0])
        geometry_center.paint_uniform_color([1,1,1])
        geometry_camera_angle.paint_uniform_color([1,1,1])
#        geometry_world_position.paint_uniform_color([0,0,0])
        geometry_object_center.paint_uniform_color([0,0,0])


        # draw lines 
        lines=[[0,1]]
        lines_angle = [[1,2],[2,3],[3,4],[4,1],
                        [0,1],[0,2],[3,0],[0,4],
                        [0,0]]
#        lines_angle = [[0,1],[1,2],[2,3],[3,0],
#             [4,5],[5,6],[6,7],[7,4],
#             [0,4],[1,5],[2,6],[3,7]]
        
#        pts3d = compute_3d_box_lidar_coords(xyz, hwl, angles=r_y, origin=(0.5,0.5,0.5), axis=2)

        box = np.array([zNear/2+center[0], zNear/2+center[1], center[-1]*ratio], dtype=np.float64)#left-down
        box2 = np.array([-1*zNear/2+center[0], zNear/2+center[1], center[-1]*ratio],dtype=np.float64)#right-down
        box3 = np.array([-1*zNear/2+center[0], -1*zNear/2+center[1], center[-1]*ratio],dtype=np.float64)#right-top
        box4 = np.array([zNear/2+center[0], -1*zNear/2+center[1], center[-1]*ratio],dtype=np.float64)#left-top
        angle_position = np.array([box,box2,box3, box4,camera_position], dtype=np.float64)


        # draw box 
        box = [4.481686, 5.147319, -1.0229858, 0.5, 0.5, 0.5121397, 1.5486346]
        xyz = camera_position#np.array(center[-1])#np.array([box[: 3]])
        hwl = np.array([box[3: 6]])
        r_y = [box[6]]

        pts3d = compute_3d_box_lidar_coords(xyz, hwl, angles=r_y, origin=(0.5,0.5,1), axis=2)

        lines_box = [[0,1],[1,2],[2,3],[3,0],
             [4,5],[5,6],[6,7],[7,4],
             [0,4],[1,5],[2,6],[3,7]]

        # draw box2
        box2 = [4.481686, 5.147319, -1.0229858, -1*zFar, -1*zFar, 0.5121397, 1.5486346]
        xyz2 = camera_position#np.array(center[-1])#np.array([box[: 3]])
        hwl2 = np.array([box2[3: 6]])
        r_y2 = [box2[6]]
        pts3d_far = compute_3d_box_lidar_coords(xyz2, hwl2, angles=r_y2, origin=(0.5,0.5,1), axis=1)
        lines_box_far = [[0,1],[1,2],[2,3],[3,0]]


        # draw object center line 
        colors = [[1,0, 0] for i in range(len(lines))]
        line_set_.points = o3d.utility.Vector3dVector([camera_position, center])
        line_set_.lines = o3d.utility.Vector2iVector(lines)
        line_set_.colors = o3d.utility.Vector3dVector(colors)
        

        # draw camera angle line near 
        colors = [[1,0, 0] for i in range(len(lines_angle))]
        line_set.points = o3d.utility.Vector3dVector(angle_position)
        line_set.lines = o3d.utility.Vector2iVector(lines_angle)
        line_set.colors = o3d.utility.Vector3dVector(colors)


        # draw camera angle line far
        colors = [[1,0, 0] for i in range(len(lines_angle))]
        line_set_far.points = o3d.utility.Vector3dVector(pts3d_far[0])#angle_position)
        line_set_far.lines = o3d.utility.Vector2iVector(lines_box_far)#lines_angle)
        line_set_far.colors = o3d.utility.Vector3dVector(colors)


        #geometry_line.points = line_set_
        if i == 0:
            vis.add_geometry(geometry)
            vis.add_geometry(geometry_camera)
#            vis.add_geometry(geometry_center)
#            vis.add_geometry(geometry_camera_angle)
#            vis.add_geometry(geometry_world_position)
            vis.add_geometry(geometry_object_center)
#            vis.add_geometry(line_set_) # object angle point
            vis.add_geometry(line_set)
#            vis.add_geometry(line_set_far)
        else:
            vis.update_geometry(geometry)
            vis.update_geometry(geometry_camera)
#           vis.update_geometry(geometry_center)
#            vis.update_geometry(geometry_camera_angle)
#            vis.update_geometry(geometry_world_position)
            vis.update_geometry(geometry_object_center)
#            vis.update_geometry(line_set_) # object angle point
            vis.update_geometry(line_set)
#            vis.update_geometry(line_set_far)

        vis.poll_events()
        vis.update_renderer()
#        time.sleep(1/20)
    vis.destroy_window()


    
#from open3d.open3d.geometry import voxel_down_sample
#source_raw = o3d.io.read_point_cloud("./output.pcd")
#source = voxel_down_sample(source_raw,voxel_size=0.02)
#print(type(source))







    
