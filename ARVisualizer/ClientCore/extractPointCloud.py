
import matplotlib.pyplot as plt
import numpy as np

class Extractor:

    def __init__(self, mode):
        self.mode = mode

    def extractPoints(self):
        # from xcode data to preprocessed data
        with open('tmp_.txt', 'r') as f:
            lines=f.readlines()

        new_list=[]
        new_list_camera_position=[]
        data=[] 
        feature=[]
        
        world=[]
        world_tmp=[]
        final_world=[]
        
        position_counter=0
        index_counter=0
        tmp=[]
        camera_position=[]

        frustum=[]
        splited_frustum=[]
        final_frustum=[]

        camera_angle=[]
        tmp_angle=[]
        final_angle=[]

        for i in lines:
            
            li=i.split(' ')

            # get camera position 
            if li[0] == "camera_position":#"thissss":
                new=i.strip('camera_position SIMD3<Float>').strip('\n').strip('(').strip(')').split(',')
                data_=[]
                try:
                    for i in new:
                        data_.append(float(i))
                except ValueError:
                    pass
                data.append(data_)
                camera_position.append(data_)
                position_counter+=1
                if position_counter == 5 or 6:
                    index_counter+=1
                    new_list_camera_position.append(camera_position)
                    camera_position=[]
                    
            # get point cloud 
            if li[0] == 'point':
                new=i.strip('point SIMD3<Float>').strip('\n').strip('(').strip(')').split(',')
                data_f=[]
                try:
                    for i in new:
                        data_f.append(float(i))
                except ValueError:
                    pass
                feature.append(data_f)
                tmp.append(data_f)
                if position_counter == 5 or 6:
                    print(index_counter)
                    new_list.append(tmp)
                    tmp=[]
                    position_counter=0

            # get xFov, yFov, zNear, zFar
            if li[0] ==  "xFov,":
                new=i.strip("xFov, yFov, zNear, zFar").strip('\n').split(' ')
                data_frustum = []
                xFov = float(new[0])
                yFov = float(new[1])
                zNear = float(new[2])
                zFar = float(new[3])
                frustum.append([xFov, yFov, zNear, zFar])
                splited_frustum.append([xFov, yFov, zNear, zFar])
                if position_counter == 5 or 6:
                    final_frustum.append(splited_frustum)
                    splited_frustum=[]

            # get angle
            if li[0] == "camera_angle":
                new = i.strip("camera_angle SIMD3<Float>").strip('\n').strip('(').strip(')').split(',')
                data_angle = []
                try:
                    for idx, i in enumerate(new):
                        if idx == 3:
                            continue
                        data_angle.append(float(i))
                except ValueError:
                    pass
                camera_angle.append(data_angle)
                tmp_angle.append(data_angle)
                if position_counter == 5 or 6:
                    final_angle.append(tmp_angle)
                    tmp_angle=[]
                
            if li[0] == 'world':
                new=i.strip('world SIMD3<Float>').strip('\n').strip('(').strip(')').split(',')
                data_world=[]
                try:
                    for idx, i in enumerate(new):
                        if idx ==3:
                            continue
                        data_world.append(float(i))
                except ValueError:
                    pass
                world.append(data_world)
                world_tmp.append(data_world)
                if position_counter == 5 or 6:
                    final_world.append(world_tmp)
                    world_tmp=[]
                
                    
            if li[0] == 'flo':
                new=i.strip('aaaaaaa simd_float4x4').strip('\n').strip('(').strip(')').split(',')
                data_=[]
                try:
                    for i in new:
                        data_.append(float(i))
                except ValueError:
                    pass
                data.append(data_)

        del feature[-1]

        array = np.array(feature, dtype=np.float64)#array, dtype=np.float64)
        center = np.array(feature, dtype=np.float64)#center, dtype=np.float64)

        array_cam=[]
        """    
        for i,d,e in zip(x,y,z):
            array_cam.append([i,d,e])
        """
        array_cam = np.array(data, dtype=np.float64)#array_cam, dtype=np.float64)

        array_w=[]
        """    
        for i,d,e in zip(x_w,y_w,z_w):
            array_w.append([i,d,e])
        """
        array_w = np.array(world, dtype=np.float64)#array_w, dtype=np.float64)

        final_frustum = np.array(final_frustum, dtype=np.float64)

        return array, array_cam, array_w, center, new_list, new_list_camera_position, final_frustum, final_angle, final_angle#final_world
