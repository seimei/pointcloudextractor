
<html>
  <body>
    <div>
      <h1>PointCloud Extractor</h1>

      $python ARVisualizer/ClientCore/getAnimation.py
</di>

<div>
  <h2>Files</h2>
  <ul>
  <li>ARVisualizer/ClientCore/getAnimation.py: get worldmap</li>
  <li>ARVisualizer/ClientCore/tmp.txt : point cloud data</li>
  </ul>
</div
  >
<div>
  <h2>Dependencies</h2>
  <ul>
  <li>Python3.6</li>
  <li>open3d==0.9.0</li>
  </ul>
</div>
</body>
</html>

